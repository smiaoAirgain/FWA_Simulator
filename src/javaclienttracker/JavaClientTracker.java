//
// Author: Sasan Ardalan
// December 2022
// (c) 2021-2022  IONOTROICS 
//

package javaclienttracker;

import java.net.*;
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class JavaClientTracker {
    
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    
    //public String command;
    //public Writer outToWS;
     private String ei_unique_id;
     private String rx_traffic;
    
    
     public void ProcessCommand(String command, Writer out) {
      
      System.out.println("Prcessing Command:" + command);
      
      //flash_greev_led ,wssid=74486b2d,!  alive,!

      String [] parseArray = command.split("!");
      System.out.println("Extracted Command:"+parseArray[0]);
      String [] parameterArray = command.split(",");
      
      //
      //extract wssid
      //
      String wssid="unknown";
      for(int i=1; i<parameterArray.length; i++) {
          System.out.println("Parameter"+parameterArray[i]);
          if(parameterArray[i].contains("wssid")){
              System.out.println("Found wsssid"+parameterArray[i]);
              String [] wssidArray= parameterArray[i].split("=");
              wssid=wssidArray[1];
              wssid=wssid.trim();
              System.out.println("zzzzuuuzzz   wsssid="+wssid);
              
          }
          
      }
      
      
      String theCommand= new String(parameterArray[0]);
     
     System.out.println("Not Trimmed:"+theCommand+ "<-Trimmed:"+theCommand.trim()+"<-");
    String checkCommand=theCommand.trim();
   //String checkCommand="get_unique_id";
   System.out.println("++++");
   System.out.println("compare:"+checkCommand.compareTo("get_unique_id"));
   if(false) {
     if(checkCommand.compareTo("get_unique_id")==0) {
           try {
                 String reponseBack=new String(ei_unique_id + " "+wssid + "^"+" ; \r\n");
                 out.write(ei_unique_id + " wssid="+wssid + "^"+" ; \r\n");
                 out.flush();
                 System.out.println(reponseBack);
                 System.out.println("XXXXXXX->"+ei_unique_id+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
         
     }
   }
     switch(checkCommand) {
         case "get_unique_id":
      //    case "abc":
              try {
                 String reponseBack=new String(ei_unique_id + " "+wssid + "^"+" ; \r\n");
                 out.write("get_unique_id="+ei_unique_id + " wssid="+wssid + "^"+" ; \r\n");
                 out.flush();
                 System.out.println(reponseBack);
                 System.out.println("XXXXXXX->"+ei_unique_id+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;
         case "get_dynamic_ip":
      //    case "abc":
             try {
                 System.out.println("Dynamicc IP addr: " + InetAddress.getLocalHost().getHostAddress());
                 try {
                       String dynamic_ip=InetAddress.getLocalHost().getHostAddress();
                        String reponseBack=new String(dynamic_ip + " "+wssid + "^"+" ; \r\n");
                             out.write(dynamic_ip + " wssid="+wssid + "^"+" ; \r\n");
                             out.flush();
                             System.out.println(reponseBack);               
                             out.write(dynamic_ip+ ";");
                             out.flush();
                  } catch (IOException ex){System.err.println(ex);}
              
             }  catch (IOException ex){System.err.println(ex);}
             
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;

         case "get_rxtraffic":
      //    case "abc":
             try {
                 String reponseBack=new String(rx_traffic + " "+wssid + "^"+" ; \r\n");
                 out.write(rx_traffic + " wssid="+wssid + "^"+" ; \r\n");
                 out.flush();
                 System.out.println(reponseBack);
                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;               
              
        case "get_txtraffic":
      //    case "abc":
             try {
                 String reponseBack=new String("0987654321" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;               
              
        case "get_version":
      //    case "abc":
             try {
                 String reponseBack=new String("fWA daemon 1.2.3.4.5" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;          
        case "get_ei_iccid":
      //    case "abc":
             try {
                 String reponseBack=new String("89011004300085700000" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;          
        case "get_ei_imsi":
      //    case "abc":
             try {
                 String reponseBack=new String("313100008574344" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;           
        case "get_ei_imei":
      //    case "abc":
             try {
                 String reponseBack=new String("863109050005086" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;              
    case "get_prim_band":
      //    case "abc":
             try {
                 String reponseBack=new String("41" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;                  
        case "get_ca_band":
      //    case "abc":
             try {
                 String reponseBack=new String("24,45,6,11" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;      
        case "get_rtrssi":
      //    case "abc":
             try {
                 String reponseBack=new String("-60" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;      
        case "get_ei_rsrp":
      //    case "abc":
             try {
                 String reponseBack=new String("-89" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;   
        case "get_ei_rsrq":
      //    case "abc":
             try {
                 String reponseBack=new String("-60" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;         
        case "get_ei_sinr":
      //    case "abc":
             try {
                 String reponseBack=new String("14" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;      
        case "get_tx_power":
      //    case "abc":
             try {
                 String reponseBack=new String("22" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;           
        case "get_qtemp":
      //    case "abc":
             try {
                 String reponseBack=new String("65" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;     
        case "get_carrier":
      //    case "abc":
             try {
                 String reponseBack=new String("verizon" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;          
        case "get_device_type":
      //    case "abc":
             try {
                 String reponseBack=new String("FWA" + " "+wssid + "^"+" ; \r\n");
                 out.write(reponseBack);
                 out.flush();
                 System.out.println(reponseBack);
//                 System.out.println("XXXXXXX->"+rx_traffic+"<-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxXXX:");
              } catch (IOException ex){System.err.println(ex);}
             // try{Thread.sleep(100);}catch(InterruptedException e){System.out.println(e);}
              System.out.println("XXXXwwwwwwwwwwwwwwwwwwwwwwwwXXX");
              break;              
        
              
              
              
          default:
              System.out.println("Unkown Command");
              
              break;
              
              
          
          
      }
     
    // theCommand=new String("abc");
      
      
      //outToWS
      
  }
    
  public JavaClientTracker() {
      StringBuffer buf = new StringBuffer ();
    int c;
    int port;
    String hostname;
   
    
    PrefFiles clientSettings = new PrefFiles("./client_settings.txt");
    clientSettings.LoadPref();

    System.out.println(clientSettings.GetPrefValue("Test"));

    Integer portInt=new Integer(clientSettings.GetPrefValue("Host_Port"));
		  port=portInt.intValue();

    hostname=clientSettings.GetPrefValue("Host");             
                  
    ei_unique_id=clientSettings.GetPrefValue("ei_unique_id");
    rx_traffic = clientSettings.GetPrefValue("rx_traffic");
    
    
 
    Socket socket = null;
    try {
  //    socket = new Socket(hostname, 28777);
  socket = new Socket(hostname, port);
      // socket.setSoTimeout(15000);
      socket.setSoTimeout(200000);
      InputStream in = socket.getInputStream();
       Writer out = new OutputStreamWriter(socket.getOutputStream());
       
       String eiUniqueId=new String("ei_unique_id="+ei_unique_id);
       String rxTraffic = new String("rx_traffic="+rx_traffic);
       
       out.write(eiUniqueId +"\r\n");
       out.flush();
       
       out.write(rxTraffic+"\r\n");
       out.flush();
       
       out.write("\r\n");
       out.flush();

  //xxx    StringBuilder time = new StringBuilder();
 //xxxx     InputStreamReader reader = new InputStreamReader(in, "ASCII");
  //+++++++++++++++++++++++++++++++++++
      while((c=in.read()) != -1) {

                                    buf.append((char)c);
                           //System.out.println("c="+c);
                                    if (c== '\n' || c== '\r') {
                                        
                                                 System.out.println("buf="+buf);
                                                 String receivedFromServer = new String(buf.toString());
                                                 if(receivedFromServer.contains("alive,!") && receivedFromServer.contains("wssid")==false  ) {
                                                     
                                                     out.write("eiAlive" +"\r\n");
                                                     out.flush();

                                          
                                                    buf.setLength(0);
                                                     
                                                 } else if(receivedFromServer.contains("wssid") ){
                                                     System.out.println("Command->"+buf+"<-");
                                                     String command = new String(buf);
                                                    //  out.write(receivedFromServer+"+++++++++++++++\r\n");
                                                   //  out.flush();
                                                     buf.setLength(0);
                                                    
                                                    
                                                     
                                                    
  
                                                     
                                                     ProcessCommand(command,out);
                                                     
                                                 }
                                                 
                                                 
                                                String [] nameValue;
                                             //   String receivedFromServer = new String(buf.toString());
                                                if(receivedFromServer.startsWith("timestamp")) {
                                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                                   System.out.println("Client  Receieved Timestamp:"+receivedFromServer+" Client Timestamp="+sdf.format(timestamp));
                                                   nameValue=receivedFromServer.split("=");
                                                   System.out.println("Timestamp Is:"+nameValue[1]);
                                                   String timeStamp= new String(nameValue[1]);
                                                   timeStamp=timeStamp.trim();
                                                    
                                                   System.out.println("xxxxwwwwxxxx ->"+timeStamp+"<-");
                                                   
                                                   
                                                   
                                                    //+++++++++++++++  Alert new Connection   +++++++++++++++++++
                                                    timestamp = new Timestamp(System.currentTimeMillis());
 
                                            //        String alertOpen= new String();
                                             //       alertOpen =alertOpen+":::alert,alertType=open,eiUniqueID="+" Client "+",timestamp="+sdf.format(timestamp);
                       
                                             //       System.out.println("Alert  open event:"+alertOpen);
                                                
                   
                                           //         out.write(buf.toString().getBytes());
                                                    // out.write(c);
                                                    out.flush();

                                          
                                                    buf.setLength(0);
                                                 

                                             } else {
                                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                                    String response=buf.toString();
                                                    response=response+" From Client timestamp="+sdf.format(timestamp);
                                                //xxx     out.write(response);
                                                    // out.write(c);
                                                    out.flush();

                                          
                                                     buf.setLength(0);
                                                    
                                             }

                  

                                }
                                    
                                    
                  }
  
  
  //+++++++++++++++++++++++++++++++++++
      
      
  //xxxx    for (int c = reader.read(); c != -1; c = reader.read()) {
  //xxxx      time.append((char) c);
      
      //System.out.println(time);
    } catch (IOException ex) {
      System.err.println(ex);
    } finally {
      if (socket != null) {
        try {
          socket.close();
        } catch (IOException ex) {
          // ignore
        }
      }
    }
      
      
  }  

  public static void main(String[] args) {

 //   String hostname = args.length > 0 ? args[0] : "time.nist.gov";
 
  //  String hostname = args.length > 0 ? args[0] : "127.0.0.1";
  
  //String hostname = args.length > 0 ? args[0] : "1.tcp.ngrok.io";
    JavaClientTracker jtracker= new JavaClientTracker();
     
    
  }
  
 
  
  
}