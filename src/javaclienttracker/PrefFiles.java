
package javaclienttracker;

 

/*  
=============================================================
Class: PrefFiles

This class will read in a set of preferences from a file 
=============================================================
*/
import java.io.*;
import java.util.*;

class PrefFiles{
        private static Properties config;
        private FileInputStream fis;
        private static String fileName ;       
    
  public  PrefFiles (String ifilename){
         fileName = ifilename;
         
  }
           
  public boolean LoadPref() {
       try {
    
            fis = new FileInputStream(fileName);
            
            config = new Properties();
            config.load(fis);
            
            
            //config.list(System.out);
            
          } catch (Exception IOException) {
             System.out.println("File: not found");
             //System.out.println(IOException/*ioA*/);
             return (false);
        }
   		return (true); 
  }
  
  public String GetPrefValue (String iPrefName){
      
      System.out.println("config="+config);
         String retVal;
         retVal= config.getProperty(iPrefName, "");
         return (retVal);
    
  }
  
  /* public static void main (String args[]){
     
     System.out.println("Running...");      
     PrefFiles aC = new PrefFiles("marks.cfg");
        
     aC.LoadPref();
  
     System.out.println("DBUSER ===>"+ aC.getPrefValue("DBUSER"));
     
     System.out.println("DUMMYUSER ===>"+ aC.getPrefValue("DUMMY"));
     
    }
    
    */
    
}
